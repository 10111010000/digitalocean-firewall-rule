package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/digitalocean/godo"
	"log"
	"os"
	"strconv"
	"time"
)

type DO struct {
	c         *godo.Client
	ctx       context.Context
	port      string
	name      string
	direction string
}

func (d DO) fireWallList() (list []godo.Firewall, response *godo.Response, err error) {
	return d.c.Firewalls.List(d.ctx, &godo.ListOptions{
		Page:         0,
		PerPage:      0,
		WithProjects: false,
	})
}

func (d DO) addFireWallRule(firewall *godo.Firewall) (response *godo.Response, err error) {
	fmt.Printf("adding firewall rule: Name= %s Port= %s Direction= %s\n", d.name, d.port, d.direction)
	switch d.direction {
	case "inbound":
		firewall.InboundRules = append(firewall.InboundRules, godo.InboundRule{
			Protocol:  "tcp",
			PortRange: d.port,
			Sources: &godo.Sources{
				Addresses: []string{"0.0.0.0/0"}, // Allow all incoming connections.
			},
		})
	case "outbound":
		firewall.OutboundRules = append(firewall.OutboundRules, godo.OutboundRule{
			Protocol:  "tcp",
			PortRange: d.port,
			Destinations: &godo.Destinations{
				Addresses: []string{"0.0.0.0/0"}, // Allow all outgoing connections.
			},
		})
	}
	return d.updateFirewall(firewall)
}

func (d DO) rmFirewallRule(firewall *godo.Firewall) (response *godo.Response, err error) {
	fmt.Printf("removing firewall rule: Name= %s Port= %s Direction= %s\n", d.name, d.port, d.direction)
	switch d.direction {
	case "inbound":
		for i, rule := range firewall.InboundRules {
			if rule.Protocol == "tcp" && rule.PortRange == d.port {
				firewall.InboundRules = append(firewall.InboundRules[:i], firewall.InboundRules[i+1:]...)
				break
			}
		}
	case "outbound":
		for i, rule := range firewall.OutboundRules {
			if rule.Protocol == "tcp" && rule.PortRange == d.port {
				firewall.OutboundRules = append(firewall.OutboundRules[:i], firewall.OutboundRules[i+1:]...)
				break
			}
		}
	}
	response, err = d.updateFirewall(firewall)
	if err != nil {
		return
	}
	return
}

func (d DO) updateFirewall(firewall *godo.Firewall) (response *godo.Response, err error) {
	fmt.Println("updating firewall")
	_, response, err = d.c.Firewalls.Update(d.ctx, firewall.ID, &godo.FirewallRequest{
		Name:          firewall.Name,
		InboundRules:  firewall.InboundRules,
		OutboundRules: firewall.OutboundRules,
		DropletIDs:    firewall.DropletIDs,
		Tags:          firewall.Tags,
	})
	if response.StatusCode != 200 {
		fmt.Println("failed to update firewall")
		fmt.Println(err)
		return response, nil
	}
	return
}

func printJson(data interface{}) {
	j, err := json.MarshalIndent(data, "", " ")
	if err != nil {
		panic(err)
	}
	fmt.Printf("%v", string(j))
}

func printHelp() {
	fmt.Print(`
dofirewall (add|rm|temp|list) <opts>
    
    temp : open port for number of seconds
        usage: dofirewall temp ${name_of_firewall} (inbound|outbound) ${port} ${timeout_int} ${api_key}
            example: dofirewall temp website inbound 22 10 api_key

    add : add firewall rule
        dofirewall add ${name_of_firewall} (inbound|outbound) ${port} ${api_key}        
            example: dofirewall add website inbound 22 api_key

    rm : remove firewall rule
        dofirewall rm ${name_of_firewall} (inbound|outbound) ${port} ${api_key}       
            example: dofirewall rm website inbound 22 api_key

    list : print firewall names       
            example: dofirewall list api_key
`)
}

func main() {
	var timeout = 0
	if len(os.Args) <= 3 {
		printHelp()
		os.Exit(2)
	}
	do := DO{}
	do.c = godo.NewFromToken(os.Args[len(os.Args)-1])
	do.ctx = context.Background()
	firewalls, _, err := do.fireWallList()
	if err != nil {
		log.Panic(err)
	}
	a, r, l, t := false, false, false, false
	switch os.Args[1] {
	case "temp":
		t2, err := strconv.ParseInt(os.Args[4], 10, 64)
		if err != nil {
			panic(err)
		}
		timeout = int(t2)
		t, a, r = true, true, true
	case "add":
		a = true
	case "rm":
		r = true
	case "list":
		l = true
	}
	if !l {
		do.name = os.Args[2]
		do.direction = os.Args[3]
		do.port = os.Args[4]
	}
	for _, firewall := range firewalls {
		if l {
			fmt.Println(firewall.Name)
			printJson(firewall)
		}
		if do.name == firewall.Name {
			if a {
				if addRes, addErr := do.addFireWallRule(&firewall); addErr != nil {
					log.Printf("%v\n", addRes)
					log.Printf("%v\n", addErr)
				}
			}
			if t {
				for i := timeout; i > 0; i-- {
					fmt.Printf("%d   \r", i)
					time.Sleep(time.Second)
				}
			}
			if r {
				if rmRes, rmErr := do.rmFirewallRule(&firewall); rmErr != nil {
					log.Printf("%v\n", rmRes)
					log.Printf("%v\n", rmErr)
				}
			}
		}
	}
}
