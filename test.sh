#!/bin/bash
api_key="dop_v1_****************************************************************"
firewall_name="website"
direction="inbound"
case "$1" in
  "add")
    echo "testing action add"
    go run . add ${firewall_name} ${direction} 22 ${api_key}
    ;;
  "rm")
    echo "testing action rm"
    go run . rm ${firewall_name} ${direction} 22 ${api_key}
    ;;
  "list")
    echo "testing action list"
    go run . list 22 ${api_key}
    ;;
  "temp")
      echo "testing action temp"
      go run . temp ${firewall_name} ${direction} 22 10 ${api_key}
      ;;
  "fail")
      echo "testing no args"
      go run .
      ;;
  *)
    echo "Usage: $0 (add|rm|temp|list)"
    exit 1
    ;;
esac