# digitalocean-firewall-rule

```shell
dofirewall (add|rm|temp|list) <opts>
    
    temp : open port for number of seconds
        usage: dofirewall temp ${name_of_firewall} (inbound|outbound) ${port} ${timeout_int} ${api_key}
            example: dofirewall temp website inbound 22 10 api_key

    add : add firewall rule
        dofirewall add ${name_of_firewall} (inbound|outbound) ${port} ${api_key}        
            example: dofirewall add website inbound 22 api_key

    rm : remove firewall rule
        dofirewall rm ${name_of_firewall} (inbound|outbound) ${port} ${api_key}       
            example: dofirewall rm website inbound 22 api_key

    list : print firewall names       
            example: dofirewall list api_key
```

* list
* add
* remove
* temporary add rule for timeout

