#!/bin/bash
programName="dofirewall"
function build() {
    CGO_ENABLED=0 go build -v -o ${programName} -ldflags="-s -w" main.go
}
function installBinary() {
    build
    sudo cp ${programName} /usr/bin/
}
function install() {
    installBinary

}
function update() {
    installBinary
}
case "$1" in
    "install")
        install;;
    "update")
        update;;
    "build")
        build;;
    *)
        printf "%s\n" "bash installer.sh (install|update|build)";;
esac
